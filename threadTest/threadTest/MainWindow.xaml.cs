﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace threadTest
{
    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }

        //Case A- Arrive 5 : DELAY(10) : Arrive 5 : DELAY(10) : Arrive 5 : DELAY(10) : Arrive 5
        private void btnA_Click(object sender, RoutedEventArgs e)
        {
            Boolean x = true;
            Boolean y = true;
            Person entrant = new Person();
            Line waitList = new Line();
            List<Person> bathroom = new List<Person>();

            //variables for setting entrants
            int num = 0;
            char gen = 'F';
            double wait;
            int globalTime = 0;

            //important variables for counters
            int counter = 0;
            int caseNumber = 1;
            char bathroomGenCheck = 'U';
            int bathroomCounter = 0;
            int lineCounter = 0;
            int departureCount = 0;
            int loops = 0;
            int delayCount = 0;

            Random random = new Random();
            int randGen;

            while (x is true)
            {
                if (delayCount > 0)
                {
                    txtOutput.Text += "DelayCount is delaying shite\n";
                }
                if (waitList.crapLine.Count <= 20 && delayCount == 0)
                {
                    if (globalTime == 0)
                    {
                        while (y is true)
                        {
                            //finds rand value between 0 and 1, sets M or F based on input
                            randGen = random.Next(0, 2);

                            if (randGen == 1)
                            {
                                gen = 'F';
                                //txtOutput.Text += "Randgen returns 1. gen = " + gen + "\n";
                            }
                            else if (randGen == 0)
                            {
                                gen = 'M';
                                //txtOutput.Text += "Randgen returns 0. gen = " + gen + "\n";
                            }

                            //finds rand value between 3 and 7
                            wait = random.Next(3, 8);

                            //creates new person and adds to list with random variables. List is the line of the people outside the bathroom
                            waitList.crapLine.Add(new Person() { gender = gen, number = num, waitTime = wait });
                            txtOutput.Text += "Time = " + globalTime + "; Person " + waitList.crapLine[waitList.crapLine.Count - 1].number + " (" + waitList.crapLine[waitList.crapLine.Count - 1].gender + ") arrives\n";

                            //txtOutput.Text += "ID: " + num + " Gender: " + gen + " Duration: " + wait + "\n";

                            num += 1;
                            if (num == 4)
                            {
                                y = false;

                            }
                        }
                        delayCount = 10;
                    }
                    //finds rand value between 0 and 1, sets M or F based on input
                    randGen = random.Next(0, 2);

                    if (randGen == 1)
                    {
                        gen = 'F';
                        //txtOutput.Text += "Randgen returns 1. gen = " + gen + "\n";
                    }
                    else if (randGen == 0)
                    {
                        gen = 'M';
                        //txtOutput.Text += "Randgen returns 0. gen = " + gen + "\n";
                    }

                    //finds rand value between 3 and 7
                    wait = random.Next(3, 8);

                    //creates new person and adds to list with random variables. List is the line of the people outside the bathroom
                    waitList.crapLine.Add(new Person() { gender = gen, number = num, waitTime = wait });
                    txtOutput.Text += "Time = " + globalTime + "; Person " + waitList.crapLine[waitList.crapLine.Count - 1].number + " (" + waitList.crapLine[waitList.crapLine.Count - 1].gender + ") arrives\n";

                    //txtOutput.Text += "ID: " + num + " Gender: " + gen + " Duration: " + wait + "\n";

                    num += 1;
                }

                else //if the line has 10 peeps in it
                {

                    if (bathroom.Count == 0)
                    {
                        caseNumber = 1;
                        //No one in bathroom, accept new entrants
                    }
                    else if (bathroom.Count == 1)
                    {
                        caseNumber = 2;
                        if (bathroom[0].gender == 'M')
                        {
                            //1 male in bathroom. Scan for more male entrants.
                            bathroomGenCheck = 'M';
                            //txtOutput.Text += "Bathroom GenCheck set to male \n";

                        }
                        else if (bathroom[0].gender == 'F')
                        {
                            //1 female in bathroom. Scan for more female entrants.
                            bathroomGenCheck = 'F';
                            //txtOutput.Text += "Bathroom GenCheck set to female \n";
                        }


                    }
                    else if (bathroom.Count == 2)
                    {

                        caseNumber = 3;
                        if (bathroom[0].gender == 'M')
                        {
                            //2 males in bathroom. Dont scan for more male entrants. Check next if male to accept 3rd, or stop process
                            //till free
                            bathroomGenCheck = 'M';

                        }
                        else if (bathroom[0].gender == 'F')
                        {
                            //2 females in bathroom. Dont scan for more female entrants. Check next if male to accept 3rd, or stop process
                            //till free
                            bathroomGenCheck = 'F';
                        }
                    }
                    else if (bathroom.Count == 3)
                    {
                        caseNumber = 4;
                        //bathroom is full. Dont search or add entrants
                    }

                    switch (caseNumber)
                    {

                        case 1: //Empty Bathroom
                            bathroom.Add(waitList.crapLine[0]);
                            txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") enters the facilities for " + waitList.crapLine[0].waitTime + " minutes\n";
                            waitList.crapLine.RemoveAt(0);
                            break;
                        case 2: //1 entrant
                            if (bathroomGenCheck == 'M')
                            {
                                lineCounter = 0; //counts index of line
                                foreach (Person waiting in waitList.crapLine)
                                {
                                    if (waitList.crapLine[lineCounter].gender == 'M') //If next person in line is male, add that boi to the bathroom
                                    {
                                        bathroom.Add(waitList.crapLine[lineCounter]);
                                        txtOutput.Text += "Person " + waitList.crapLine[lineCounter].number + " (" + waitList.crapLine[lineCounter].gender + ") enters the facilities for " + waitList.crapLine[lineCounter].waitTime + " minutes\n";
                                        waitList.crapLine.RemoveAt(lineCounter);
                                        //txtOutput.Text += "Got to the break\n";
                                        break;
                                    }
                                    else if (waitList.crapLine[lineCounter].gender == 'F') //If the next person in line is female, deny her entry. There be males in thar
                                    {
                                        lineCounter += 1;
                                    }
                                }
                            }
                            else if (bathroomGenCheck == 'F')
                            {
                                foreach (Person waiting in waitList.crapLine)
                                {
                                    lineCounter = 0; //counts index of line
                                    if (waitList.crapLine[lineCounter].gender == 'F') //If next person in line is female, add that gurl into the bathroom
                                    {
                                        bathroom.Add(waitList.crapLine[lineCounter]);
                                        txtOutput.Text += "Person " + waitList.crapLine[lineCounter].number + " (" + waitList.crapLine[lineCounter].gender + ") enters the facilities for " + waitList.crapLine[lineCounter].waitTime + " minutes\n";
                                        waitList.crapLine.RemoveAt(lineCounter);
                                        //txtOutput.Text += "Got to the break\n";
                                        break;
                                    }
                                    else if (waitList.crapLine[lineCounter].gender == 'M') //If the next person in line is male, deny that man entry. There be girls in thar
                                    {
                                        lineCounter += 1;
                                    }
                                }

                            }
                            break;
                        case 3: //2 entrant
                            if (bathroomGenCheck == 'M')
                            {
                                if (waitList.crapLine[0].gender == 'M')
                                {
                                    bathroom.Add(waitList.crapLine[0]);
                                    txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") enters the facilities for " + waitList.crapLine[0].waitTime + " minutes\n";
                                    waitList.crapLine.RemoveAt(0);
                                    //txtOutput.Text += "Got to the break\n";
                                }
                                else
                                {
                                    //dont do anything. The next person in line isnt male
                                }
                            }
                            else if (bathroomGenCheck == 'F')
                            {
                                if (waitList.crapLine[0].gender == 'F')
                                {
                                    bathroom.Add(waitList.crapLine[0]);
                                    txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") enters the facilities for " + waitList.crapLine[0].waitTime + " minutes\n";
                                    waitList.crapLine.RemoveAt(0);
                                    //txtOutput.Text += "Got to the break\n";
                                }
                                else
                                {
                                    //dont do anything. The next person in line isnt female
                                }
                            }
                            break;
                        case 4: //full
                            txtOutput.Text += "Bathroom is full, waiting\n";
                            break;

                    }
                    //subtracts one off time duration in the restroom for each person in the bathroom currently
                    try
                    {
                        foreach (Person crapper in bathroom)
                        {
                            if (bathroom.Count > 1) //if more than one person is in the bathroom
                            {
                                if (bathroom[bathroomCounter].waitTime <= 1) //if the person at the current index is on his last loop in the bathroom, remove him from thine crapper
                                {
                                    departureCount += 1;
                                    txtOutput.Text += "Person " + waitList.crapLine[bathroomCounter].number + " (" + waitList.crapLine[bathroomCounter].gender + ") exits " + "(" + "departure = " + departureCount + ")\n";
                                    bathroom.RemoveAt(bathroomCounter);
                                    globalTime += 1;
                                    delayCount -= 1;

                                    if (bathroom.Count == 1)
                                    {
                                        break;
                                    }
                                }
                                else if (bathroom[bathroomCounter].waitTime >= 2) //if the person at the current index has more time left, subract one loop from his/her time
                                {
                                    bathroom[bathroomCounter].waitTime -= 1;
                                    globalTime += 1;
                                    delayCount -= 1;
                                    bathroomCounter += 1;
                                }

                            }
                            else if (bathroom.Count == 1) //if only one person is in the bathroom (Stops indexing errors)
                            {
                                if (bathroom[0].waitTime <= 1)
                                {
                                    departureCount += 1;
                                    txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") exits " + "(" + "departure = " + departureCount + ")\n";
                                    bathroom.RemoveAt(0);
                                    globalTime += 1;
                                    delayCount -= 1;
                                }
                                else if (bathroom[0].waitTime >= 2)
                                {
                                    bathroom[0].waitTime -= 1;
                                    globalTime += 1;
                                    delayCount -= 1;
                                }
                            }
                            else if (bathroom.Count == 0)
                            {
                                break;
                            }

                        }
                    }
                    catch
                    {

                    }

                    bathroomCounter = 0; //resets bathroom check counter
                    loops += 1;

                    if (loops == 40) //This number can change to allow a larger or smaller testing set. Currenly set to only test up to 20 people
                    {
                        x = false;
                    }

                }

            }
            txtOutput.Text += "------------------------Currently in line------------------------\n";
            foreach (Person thing in waitList.crapLine)
            {


                txtOutput.Text += "Gender: " + waitList.crapLine[counter].gender + " ID: " + waitList.crapLine[counter].number + "\n";
                counter += 1;
            }
        }


        //Case B: Arrive 10, delay 10, Arrive 10
        private void btnB_Click(object sender, RoutedEventArgs e)
        {
            Boolean x = true;
            Boolean y = true;
            Person entrant = new Person();
            Line waitList = new Line();
            List<Person> bathroom = new List<Person>();

            //variables for setting entrants
            int num = 0;
            char gen = 'F';
            double wait;
            int globalTime = 0;

            //important variables for counters
            int counter = 0;
            int caseNumber = 1;
            char bathroomGenCheck = 'U';
            int bathroomCounter = 0;
            int lineCounter = 0;
            int departureCount = 0;
            int loops = 0;
            int delayCount=0;

            Random random = new Random();
            int randGen;

            while (x is true)
            {
                if (delayCount > 0)
                {
                    txtOutput.Text += "DelayCount is delaying shite\n";
                }
                if (waitList.crapLine.Count <= 20 && delayCount ==0)
                {
                    if (globalTime == 0)
                    {
                        while (y is true)
                        {
                            //finds rand value between 0 and 1, sets M or F based on input
                            randGen = random.Next(0, 2);

                            if (randGen == 1)
                            {
                                gen = 'F';
                                //txtOutput.Text += "Randgen returns 1. gen = " + gen + "\n";
                            }
                            else if (randGen == 0)
                            {
                                gen = 'M';
                                //txtOutput.Text += "Randgen returns 0. gen = " + gen + "\n";
                            }

                            //finds rand value between 3 and 7
                            wait = random.Next(3, 8);

                            //creates new person and adds to list with random variables. List is the line of the people outside the bathroom
                            waitList.crapLine.Add(new Person() { gender = gen, number = num, waitTime = wait });
                            txtOutput.Text += "Time = " + globalTime + "; Person " + waitList.crapLine[waitList.crapLine.Count - 1].number + " (" + waitList.crapLine[waitList.crapLine.Count - 1].gender + ") arrives\n";

                            //txtOutput.Text += "ID: " + num + " Gender: " + gen + " Duration: " + wait + "\n";

                            num += 1;
                            if (num == 9)
                            {
                                y = false;
                                
                            }
                        }
                    delayCount = 10;
                    }
                    //finds rand value between 0 and 1, sets M or F based on input
                    randGen = random.Next(0, 2);

                    if (randGen == 1)
                    {
                        gen = 'F';
                        //txtOutput.Text += "Randgen returns 1. gen = " + gen + "\n";
                    }
                    else if (randGen == 0)
                    {
                        gen = 'M';
                        //txtOutput.Text += "Randgen returns 0. gen = " + gen + "\n";
                    }

                    //finds rand value between 3 and 7
                    wait = random.Next(3, 8);

                    //creates new person and adds to list with random variables. List is the line of the people outside the bathroom
                    waitList.crapLine.Add(new Person() { gender = gen, number = num, waitTime = wait });
                    txtOutput.Text += "Time = " + globalTime + "; Person " + waitList.crapLine[waitList.crapLine.Count - 1].number + " (" + waitList.crapLine[waitList.crapLine.Count - 1].gender + ") arrives\n";

                    //txtOutput.Text += "ID: " + num + " Gender: " + gen + " Duration: " + wait + "\n";

                    num += 1;
                }
                   
                else //if the line has 10 peeps in it
                {

                    if (bathroom.Count == 0)
                    {
                        caseNumber = 1;
                        //No one in bathroom, accept new entrants
                    }
                    else if (bathroom.Count == 1)
                    {
                        caseNumber = 2;
                        if (bathroom[0].gender == 'M')
                        {
                            //1 male in bathroom. Scan for more male entrants.
                            bathroomGenCheck = 'M';
                            //txtOutput.Text += "Bathroom GenCheck set to male \n";

                        }
                        else if (bathroom[0].gender == 'F')
                        {
                            //1 female in bathroom. Scan for more female entrants.
                            bathroomGenCheck = 'F';
                            //txtOutput.Text += "Bathroom GenCheck set to female \n";
                        }


                    }
                    else if (bathroom.Count == 2)
                    {

                        caseNumber = 3;
                        if (bathroom[0].gender == 'M')
                        {
                            //2 males in bathroom. Dont scan for more male entrants. Check next if male to accept 3rd, or stop process
                            //till free
                            bathroomGenCheck = 'M';

                        }
                        else if (bathroom[0].gender == 'F')
                        {
                            //2 females in bathroom. Dont scan for more female entrants. Check next if male to accept 3rd, or stop process
                            //till free
                            bathroomGenCheck = 'F';
                        }
                    }
                    else if (bathroom.Count == 3)
                    {
                        caseNumber = 4;
                        //bathroom is full. Dont search or add entrants
                    }

                    switch (caseNumber)
                    {

                        case 1: //Empty Bathroom
                            bathroom.Add(waitList.crapLine[0]);
                            txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") enters the facilities for " + waitList.crapLine[0].waitTime + " minutes\n";
                            waitList.crapLine.RemoveAt(0);
                            break;
                        case 2: //1 entrant
                            if (bathroomGenCheck == 'M')
                            {
                                lineCounter = 0; //counts index of line
                                foreach (Person waiting in waitList.crapLine)
                                {
                                    if (waitList.crapLine[lineCounter].gender == 'M') //If next person in line is male, add that boi to the bathroom
                                    {
                                        bathroom.Add(waitList.crapLine[lineCounter]);
                                        txtOutput.Text += "Person " + waitList.crapLine[lineCounter].number + " (" + waitList.crapLine[lineCounter].gender + ") enters the facilities for " + waitList.crapLine[lineCounter].waitTime + " minutes\n";
                                        waitList.crapLine.RemoveAt(lineCounter);
                                        //txtOutput.Text += "Got to the break\n";
                                        break;
                                    }
                                    else if (waitList.crapLine[lineCounter].gender == 'F') //If the next person in line is female, deny her entry. There be males in thar
                                    {
                                        lineCounter += 1;
                                    }
                                }
                            }
                            else if (bathroomGenCheck == 'F')
                            {
                                foreach (Person waiting in waitList.crapLine)
                                {
                                    lineCounter = 0; //counts index of line
                                    if (waitList.crapLine[lineCounter].gender == 'F') //If next person in line is female, add that gurl into the bathroom
                                    {
                                        bathroom.Add(waitList.crapLine[lineCounter]);
                                        txtOutput.Text += "Person " + waitList.crapLine[lineCounter].number + " (" + waitList.crapLine[lineCounter].gender + ") enters the facilities for " + waitList.crapLine[lineCounter].waitTime + " minutes\n";
                                        waitList.crapLine.RemoveAt(lineCounter);
                                        //txtOutput.Text += "Got to the break\n";
                                        break;
                                    }
                                    else if (waitList.crapLine[lineCounter].gender == 'M') //If the next person in line is male, deny that man entry. There be girls in thar
                                    {
                                        lineCounter += 1;
                                    }
                                }

                            }
                            break;
                        case 3: //2 entrant
                            if (bathroomGenCheck == 'M')
                            {
                                if (waitList.crapLine[0].gender == 'M')
                                {
                                    bathroom.Add(waitList.crapLine[0]);
                                    txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") enters the facilities for " + waitList.crapLine[0].waitTime + " minutes\n";
                                    waitList.crapLine.RemoveAt(0);
                                    //txtOutput.Text += "Got to the break\n";
                                }
                                else
                                {
                                    //dont do anything. The next person in line isnt male
                                }
                            }
                            else if (bathroomGenCheck == 'F')
                            {
                                if (waitList.crapLine[0].gender == 'F')
                                {
                                    bathroom.Add(waitList.crapLine[0]);
                                    txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") enters the facilities for " + waitList.crapLine[0].waitTime + " minutes\n";
                                    waitList.crapLine.RemoveAt(0);
                                    //txtOutput.Text += "Got to the break\n";
                                }
                                else
                                {
                                    //dont do anything. The next person in line isnt female
                                }
                            }
                            break;
                        case 4: //full
                            txtOutput.Text += "Bathroom is full, waiting\n";
                            break;

                    }
                    //subtracts one off time duration in the restroom for each person in the bathroom currently
                    try
                    {
                        foreach (Person crapper in bathroom)
                        {
                            if (bathroom.Count > 1) //if more than one person is in the bathroom
                            {
                                if (bathroom[bathroomCounter].waitTime <= 1) //if the person at the current index is on his last loop in the bathroom, remove him from thine crapper
                                {
                                    departureCount += 1;
                                    txtOutput.Text += "Person " + waitList.crapLine[bathroomCounter].number + " (" + waitList.crapLine[bathroomCounter].gender + ") exits " + "(" + "departure = " + departureCount + ")\n";
                                    bathroom.RemoveAt(bathroomCounter);
                                    globalTime += 1;
                                    delayCount -= 1;

                                    if (bathroom.Count == 1)
                                    {
                                        break;
                                    }
                                }
                                else if (bathroom[bathroomCounter].waitTime >= 2) //if the person at the current index has more time left, subract one loop from his/her time
                                {
                                    bathroom[bathroomCounter].waitTime -= 1;
                                    globalTime += 1;
                                    delayCount -= 1;
                                    bathroomCounter += 1;
                                }

                            }
                            else if (bathroom.Count == 1) //if only one person is in the bathroom (Stops indexing errors)
                            {
                                if (bathroom[0].waitTime <= 1)
                                {
                                    departureCount += 1;
                                    txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") exits " + "(" + "departure = " + departureCount + ")\n";
                                    bathroom.RemoveAt(0);
                                    globalTime += 1;
                                    delayCount -= 1;
                                }
                                else if (bathroom[0].waitTime >= 2)
                                {
                                    bathroom[0].waitTime -= 1;
                                    globalTime += 1;
                                    delayCount -= 1;
                                }
                            }
                            else if (bathroom.Count == 0)
                            {
                                break;
                            }

                        }
                    }
                    catch
                    {

                    }

                    bathroomCounter = 0; //resets bathroom check counter
                    loops += 1;

                    if (loops == 40) //This number can change to allow a larger or smaller testing set. Currenly set to only test up to 20 people
                    {
                        x = false;
                    }

                }

            }
            txtOutput.Text += "------------------------Currently in line------------------------\n";
            foreach (Person thing in waitList.crapLine)
            {


                txtOutput.Text += "Gender: " + waitList.crapLine[counter].gender + " ID: " + waitList.crapLine[counter].number + "\n";
                counter += 1;
            }

        }


        //Case C: 20 people all at once
        private void btnC_Click(object sender, RoutedEventArgs e)
        {
            Boolean x = true;
            Boolean y = true;
            Person entrant = new Person();
            Line waitList = new Line();
            List<Person> bathroom = new List<Person>();

            //variables for setting entrants
            int num = 0;
            char gen = 'F';
            double wait;
            int globalTime = 0;

            //important variables for counters
            int counter = 0;
            int caseNumber = 1;
            char bathroomGenCheck = 'U';
            int bathroomCounter = 0;
            int lineCounter = 0;
            int departureCount = 0;
            int loops = 0;

            Random random = new Random();
            int randGen;

            while (x is true)
            {
                if (waitList.crapLine.Count <= 20)
                {
                    if (globalTime == 0)
                    {
                        while (y is true)
                        {
                            //finds rand value between 0 and 1, sets M or F based on input
                            randGen = random.Next(0, 2);

                            if (randGen == 1)
                            {
                                gen = 'F';
                                //txtOutput.Text += "Randgen returns 1. gen = " + gen + "\n";
                            }
                            else if (randGen == 0)
                            {
                                gen = 'M';
                                //txtOutput.Text += "Randgen returns 0. gen = " + gen + "\n";
                            }

                            //finds rand value between 3 and 7
                            wait = random.Next(3, 8);

                            //creates new person and adds to list with random variables. List is the line of the people outside the bathroom
                            waitList.crapLine.Add(new Person() { gender = gen, number = num, waitTime = wait });
                            txtOutput.Text += "Time = " + globalTime + "; Person " + waitList.crapLine[waitList.crapLine.Count - 1].number + " (" + waitList.crapLine[waitList.crapLine.Count - 1].gender + ") arrives\n";

                            //txtOutput.Text += "ID: " + num + " Gender: " + gen + " Duration: " + wait + "\n";

                            num += 1;
                            if (num >=19)
                            {
                                y = false;
                            }
                        }

                    }
                    //finds rand value between 0 and 1, sets M or F based on input
                    randGen = random.Next(0, 2);

                    if (randGen == 1)
                    {
                        gen = 'F';
                        //txtOutput.Text += "Randgen returns 1. gen = " + gen + "\n";
                    }
                    else if (randGen == 0)
                    {
                        gen = 'M';
                        //txtOutput.Text += "Randgen returns 0. gen = " + gen + "\n";
                    }

                    //finds rand value between 3 and 7
                    wait = random.Next(3, 8);

                    //creates new person and adds to list with random variables. List is the line of the people outside the bathroom
                    waitList.crapLine.Add(new Person() { gender = gen, number = num, waitTime = wait });
                    txtOutput.Text += "Time = " + globalTime + "; Person " + waitList.crapLine[waitList.crapLine.Count - 1].number + " (" + waitList.crapLine[waitList.crapLine.Count - 1].gender + ") arrives\n";

                    //txtOutput.Text += "ID: " + num + " Gender: " + gen + " Duration: " + wait + "\n";

                    num += 1;
                }
                else //if the line has 10 peeps in it
                {

                    if (bathroom.Count == 0)
                    {
                        caseNumber = 1;
                        //No one in bathroom, accept new entrants
                    }
                    else if (bathroom.Count ==  1)
                    {
                        caseNumber = 2;
                        if (bathroom[0].gender == 'M')
                        {
                            //1 male in bathroom. Scan for more male entrants.
                            bathroomGenCheck = 'M';
                            //txtOutput.Text += "Bathroom GenCheck set to male \n";

                        }
                        else if (bathroom[0].gender == 'F')
                        {
                            //1 female in bathroom. Scan for more female entrants.
                            bathroomGenCheck = 'F';
                            //txtOutput.Text += "Bathroom GenCheck set to female \n";
                        }
                        

                    }
                    else if (bathroom.Count == 2)
                    {
                        
                        caseNumber = 3;
                        if (bathroom[0].gender == 'M')
                        {
                            //2 males in bathroom. Dont scan for more male entrants. Check next if male to accept 3rd, or stop process
                            //till free
                            bathroomGenCheck = 'M';

                        }
                        else if (bathroom[0].gender == 'F')
                        {
                            //2 females in bathroom. Dont scan for more female entrants. Check next if male to accept 3rd, or stop process
                            //till free
                            bathroomGenCheck = 'F';
                        }
                    }
                    else if (bathroom.Count == 3)
                    {
                        caseNumber = 4;
                        //bathroom is full. Dont search or add entrants
                    }
                    
                    switch (caseNumber)
                    {

                        case 1: //Empty Bathroom
                            bathroom.Add(waitList.crapLine[0]);
                            txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") enters the facilities for " + waitList.crapLine[0].waitTime + " minutes\n";
                            waitList.crapLine.RemoveAt(0);
                            break;
                        case 2: //1 entrant
                            if (bathroomGenCheck == 'M')
                            {
                                lineCounter = 0; //counts index of line
                                foreach (Person waiting in waitList.crapLine)
                                {
                                    if (waitList.crapLine[lineCounter].gender == 'M') //If next person in line is male, add that boi to the bathroom
                                    {
                                        bathroom.Add(waitList.crapLine[lineCounter]);
                                        txtOutput.Text += "Person " + waitList.crapLine[lineCounter].number + " (" + waitList.crapLine[lineCounter].gender + ") enters the facilities for " + waitList.crapLine[lineCounter].waitTime + " minutes\n";
                                        waitList.crapLine.RemoveAt(lineCounter);
                                        //txtOutput.Text += "Got to the break\n";
                                        break;
                                    }
                                    else if (waitList.crapLine[lineCounter].gender == 'F') //If the next person in line is female, deny her entry. There be males in thar
                                    {
                                        lineCounter += 1;
                                    }
                                }
                            }
                            else if (bathroomGenCheck == 'F')
                            {
                                foreach (Person waiting in waitList.crapLine)
                                {
                                    lineCounter = 0; //counts index of line
                                    if (waitList.crapLine[lineCounter].gender == 'F') //If next person in line is female, add that gurl into the bathroom
                                    {
                                        bathroom.Add(waitList.crapLine[lineCounter]);
                                        txtOutput.Text += "Person " + waitList.crapLine[lineCounter].number + " (" + waitList.crapLine[lineCounter].gender + ") enters the facilities for " + waitList.crapLine[lineCounter].waitTime + " minutes\n";
                                        waitList.crapLine.RemoveAt(lineCounter);
                                        //txtOutput.Text += "Got to the break\n";
                                        break;
                                    }
                                    else if (waitList.crapLine[lineCounter].gender == 'M') //If the next person in line is male, deny that man entry. There be girls in thar
                                    {
                                        lineCounter += 1;
                                    }
                                }
                                
                            }
                            break;
                        case 3: //2 entrant
                            if (bathroomGenCheck == 'M')
                            {
                                if (waitList.crapLine[0].gender == 'M')
                                {
                                    bathroom.Add(waitList.crapLine[0]);
                                    txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") enters the facilities for " + waitList.crapLine[0].waitTime + " minutes\n";
                                    waitList.crapLine.RemoveAt(0);
                                    //txtOutput.Text += "Got to the break\n";
                                }
                                else
                                {
                                    //dont do anything. The next person in line isnt male
                                }
                            }
                            else if (bathroomGenCheck == 'F')
                            {
                                if (waitList.crapLine[0].gender == 'F')
                                {
                                    bathroom.Add(waitList.crapLine[0]);
                                    txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") enters the facilities for " + waitList.crapLine[0].waitTime + " minutes\n";
                                    waitList.crapLine.RemoveAt(0);
                                    //txtOutput.Text += "Got to the break\n";
                                }
                                else
                                {
                                    //dont do anything. The next person in line isnt female
                                }
                            }
                            break;
                        case 4: //full
                            txtOutput.Text += "Bathroom is full, waiting\n";
                            break;

                    }
                    //subtracts one off time duration in the restroom for each person in the bathroom currently
                    try
                    {
                        foreach (Person crapper in bathroom)
                        {
                            if (bathroom.Count > 1) //if more than one person is in the bathroom
                            {
                                if (bathroom[bathroomCounter].waitTime <= 1) //if the person at the current index is on his last loop in the bathroom, remove him from thine crapper
                                {
                                    departureCount += 1;
                                    txtOutput.Text += "Person " + waitList.crapLine[bathroomCounter].number + " (" + waitList.crapLine[bathroomCounter].gender + ") exits " + "(" + "departure = " + departureCount +")\n";
                                    bathroom.RemoveAt(bathroomCounter);
                                    globalTime += 1;

                                    if (bathroom.Count == 1)
                                    {
                                        break;
                                    }
                                }
                                else if (bathroom[bathroomCounter].waitTime >= 2) //if the person at the current index has more time left, subract one loop from his/her time
                                {
                                    bathroom[bathroomCounter].waitTime -= 1;
                                    globalTime += 1;
                                    bathroomCounter += 1;
                                }

                            }
                            else if (bathroom.Count == 1) //if only one person is in the bathroom (Stops indexing errors)
                            {
                                if (bathroom[0].waitTime <= 1)
                                {
                                    departureCount += 1;
                                    txtOutput.Text += "Person " + waitList.crapLine[0].number + " (" + waitList.crapLine[0].gender + ") exits " + "(" + "departure = " + departureCount + ")\n";
                                    bathroom.RemoveAt(0);
                                    globalTime += 1;
                                }
                                else if (bathroom[0].waitTime >= 2)
                                {
                                    bathroom[0].waitTime -= 1;
                                    globalTime += 1;
                                }
                            }
                            else if (bathroom.Count == 0)
                            {
                                break;
                            }

                        }
                    }
                    catch
                    {

                    }

                    bathroomCounter = 0; //resets bathroom check counter
                    loops += 1;

                    if (loops ==20) //This number can change to allow a larger or smaller testing set. Currenly set to only test up to 20 people
                    {
                        x = false;
                    }
                    
                }
                
            }
            txtOutput.Text += "------------------------Currently in line------------------------\n";
            foreach (Person thing in waitList.crapLine)
            {
                              
                txtOutput.Text += "Gender: " + waitList.crapLine[counter].gender + " ID: " + waitList.crapLine[counter].number + "\n";
                counter += 1;
            }

            
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtOutput.Text = "";
        }

        private void btnEnd_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
